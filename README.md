# **PRAKTIKUM SISTEM OPERASI MODUL 4 FUSE dan Docker**

|    NRP     |          Nama           |
| :--------: | :---------------------: |
| 5025211010 |   Dimas Fadilah Akbar   |
| 5025211045 | Muhammad Ersya Vinorian |
| 5025211122 |   Muhammad Zien Zidan   |

</div>
<br/>

# SOAL 1 : Kaggle dan Docker

Dalam menjawab soal ini kita membutuhkan 3 file yaitu strogae.c untuk menjawab soal 1 a dan 1 b, Dockerfile untuk menjawab soal 1 c, serta docker-compose.yml
untuk menjawab soal 1 e. Selain itu, kita juga butuh file hubdocker.txt sebagai tempat menyimpan link docker hub untuk menjawab soal 1 d.

## 1.1 Mendownload file di Kaggle
``` c
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
system("unzip fifa-player-stats-database.zip");
```
disini kita diminta untuk membuat file.c yaitu storage.c bertujuan mengunduh file zip yang ada di kaggle setelah itu mengekstrak file zip tersebut.

Berikut hasil :
![image](/uploads/c05e46473b73a596548530a798a3a6d8/image.png)

## 1.2 Membaca data di file storage.c
```c
system("awk -F',' '{ if ( $3<25 && $8>85 && $9!=\"Manchester City\" ) print $2 \" - \" $9 \" - \" $3 \" - \" $8 \" - \" $4 \" - \" $14}' FIFA23_official_data.csv");
```
disini kita diminta untuk membaca data dan menganalisa yang ada didalam file FIFA23_official_data.csv

Berikut hasil :
![image](/uploads/31caa9f74e81eb00e23e9c83a8fdda8e/image.png)

## 1.3 Membuat Dockerfile
```Dockerfile
# Menggunakan base image Ubuntu 20.04
FROM ubuntu:20.04

# Mengupdate paket
RUN apt-get update

# Menginstall Python 3 dan pip
RUN apt-get install -y python3 python3-pip

# Menginstall kaggle melalui pip
RUN pip install kaggle

# Menginstall wget
RUN apt-get install -y wget

# Mengatur direktori kerja
WORKDIR /app

# Menyalin file kaggle.json ke dalam image Docker
COPY kaggle.json /root/.kaggle/kaggle.json

# Mengatur permission file kaggle.json
RUN chmod 600 /root/.kaggle/kaggle.json

# Menginstall paket yang diperlukan untuk menjalankan perintah kaggle
RUN apt-get install -y wget unzip

# Menyalin kode ke dalam image Docker
COPY storage.c /app/

# Menginstall GCC (GNU Compiler Collection)
RUN apt-get install -y gcc

# Mengkompilasi program
RUN gcc -o storage /app/storage.c

# Menjalankan program saat container dijalankan
CMD ["./storage"]
```
disini kita diminta untuk membuat Docker Container dari file storage.c

Berikut hasil :
![image](/uploads/5fa1378a02e2b999a497c14cb0e4eaa0/image.png)

## 1.4 Mengunggah ke dalam Docker Hub
```link
https://hub.docker.com/repository/docker/zienzidan/storage-app/general
```
disini kita diminta mengunggah file dari dockerfile tersebut kedalam docker hub dengan repository storage-app

Berikut hasil :
![image](/uploads/f196cd61b7306e4d35c7cd4ec0026cd4/image.png)

## 1.5 Membuat Docker Compose

### Napoli
```Docker
# Folder Napoli
version: '3'
services:
  instance1:
    image: soal1
    ports:
      - 8010:80
  instance2:
    image: soal1
    ports:
      - 8011:80
  instance3:
    image: soal1
    ports:
      - 8012:80
  instance4:
    image: soal1
    ports:
      - 8013:80
  instance5:
    image: soal1
    ports:
      - 8014:80

```
### Barcelona
```Docker
version: '3'
services:
  instance1:
    image: soal1
    ports:
      - 8015:80
  instance2:
    image: soal1
    ports:
      - 8016:80
  instance3:
    image: soal1
    ports:
      - 8017:80
  instance4:
    image: soal1
    ports:
      - 8018:80
  instance5:
    image: soal1
    ports:
      - 8019:80
```
disini kita diminta untuk membuat 5 instance dengan membuat 2 folder terpisah dan port nya harus beda 

Berikut hasil :
![image](/uploads/3ecb777dcbdad39b05bf5dbd34c773d9/image.png)

# Soal 4: FUSE and Modularization

## 4.1 Fuse Operations

### 4.1.1 getattr

```c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;
    char fpath[1000];

    strcpy(fpath, homedir);
    strncat(fpath, path, sizeof(fpath) - strlen(fpath) - 1);
    // log_message("REPORT", "GETATTR", fpath);

    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}
```

Fungsi `.getattr` pada FUSE (Filesystem in Userspace) digunakan untuk mengambil atribut (metadata) dari sebuah file atau direktori. Fungsi ini akan dipanggil oleh FUSE ketika sistem operasi ingin mendapatkan informasi tentang file atau direktori, seperti ukuran, tipe file, hak akses, pemilik, waktu modifikasi, dan lain-lain.

### 4.1.2 readdir

```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    char fpath[1000];
    static int readCount = 0;
    char newpath[2000];

    (void)offset;
    (void)fi;

    strcpy(fpath, homedir);
    strncat(fpath, path, sizeof(fpath) - strlen(fpath) - 1);
    log_message("REPORT", "READDIR", fpath);
    // printf("readdir %s\n", fpath);

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        // printf("name %s\n", de->d_name);
        sprintf(newpath, "%s%s", fpath, de->d_name);
        if (strstr(newpath, "module_") && !readCount)
        {
            printf("newpath readdir %s\n", newpath);
            listFilesRecursively(newpath, 0);
            readCount++;
            printf("readCount %d", readCount);
        }
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);
    return 0;
}

```

Fungsi `.readdir` pada FUSE (Filesystem in Userspace) digunakan untuk membaca isi dari sebuah direktori. Fungsi ini akan dipanggil oleh FUSE ketika sistem operasi ingin mengetahui daftar file dan direktori yang ada di dalam suatu direktori.

Pada fungsi `.readdir` terdapat modifikasi untuk melakukan modularisasi yaitu apabila ketika fungsi `.readdir` dipanggil maka akan melakukan pengecekan apakah terdapat direktori dengan substring `module_`, apabila direktori tersebut exists maka akan dilakukan proses spliting file menjadi ukuran 1 kilo bytes.

### 4.1.3 open

```c
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    int res = 0;
    char fpath[1000];

    strcpy(fpath, homedir);
    strncat(fpath, path, sizeof(fpath) - strlen(fpath) - 1);
    log_message("REPORT", "OPEN", fpath);
    printf("open %s\n", fpath);

    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;

    fi->fh = res;

    close(res);
    return 0;
}
```

Fungsi .open pada FUSE (Filesystem in Userspace) digunakan untuk membuka (open) sebuah file. Fungsi ini akan dipanggil oleh FUSE ketika sistem operasi ingin membuka file untuk membaca atau menulis isinya.

1. `static int xmp_open(const char *path, struct fuse_file_info *fi)`: Deklarasi fungsi xmp_open dengan parameter path yang merupakan path dari file yang akan dibuka, dan fi yang merupakan pointer ke struktur fuse_file_info yang berisi informasi tentang file yang akan dibuka.

2. `int res = 0;`: Inisialisasi variabel lokal res dengan nilai 0. Variabel ini akan digunakan untuk menyimpan nilai hasil dari operasi open dan close.

3. `char fpath[1000];`: Deklarasi array karakter fpath dengan panjang 1000. Array ini digunakan untuk menyimpan path lengkap dari file yang akan dibuka.

4. `strcpy(fpath, homedir);`: Menyalin isi dari variabel homedir ke array fpath. Ini dilakukan untuk memastikan bahwa direktori home ditambahkan pada path file yang akan dibuka.

5. `strncat(fpath, path, sizeof(fpath) - strlen(fpath) - 1);`: Menggabungkan path file yang akan dibuka ke dalam array fpath. strncat digunakan untuk menggabungkan string dengan membatasi panjang maksimum yang bisa digabungkan agar tidak terjadi overflow pada array.

6. `log_message("REPORT", "OPEN", fpath);`: Memanggil fungsi log_message untuk mencatat pesan log dengan keterangan "OPEN" dan path file yang akan dibuka.

7. `res = open(fpath, fi->flags);`: Membuka file yang ditentukan oleh fpath dengan mode yang ditentukan oleh fi->flags. Hasil dari operasi open disimpan dalam variabel res.

8. `if (res == -1) return -errno;`: Memeriksa apakah operasi open berhasil. Jika terjadi kesalahan, nilai -errno dikembalikan untuk menunjukkan jenis kesalahan yang terjadi.

9. `fi->fh = res;`: Menyimpan file descriptor hasil dari open dalam struktur fuse_file_info. File descriptor ini nantinya akan digunakan dalam operasi file lainnya.

10. `close(res);`: Menutup file yang telah dibuka dengan menggunakan file descriptor yang disimpan dalam res.

### 4.1.4 rename

```c
static int xmp_rename(const char *oldpath, const char *newpath)
{
    printf("rename %s -> %s\n", oldpath, newpath);

    int res;
    char fpath[1000];
    char fpath2[1000];
    sprintf(fpath, "%s%s", homedir, oldpath);
    sprintf(fpath2, "%s%s", homedir, newpath);
    log_rename("REPORT", "RENAME", fpath, fpath2);

    res = rename(fpath, fpath2);
    if (res == -1)
        return -errno;

    struct stat oldstat;

    if (stat(fpath2, &oldstat) == -1)
        return -errno;

    if (S_ISDIR(oldstat.st_mode))
    {
        char *result = strstr(newpath, "module_");
        if (result != NULL)
        {
            listFilesRecursively(fpath2, 0);
        }
        if (strstr(oldpath, "module_") != NULL && strstr(newpath, "module_") == NULL)
        {

            listFilesRecursively(fpath2, 1);
        }

        printf("renaming directory\n");
    }
    else
    {
        printf("renaming file\n");
    }

    return 0;
}

```

Fungsi .rename pada FUSE (Filesystem in Userspace) digunakan untuk mengubah nama (rename) sebuah file atau direktori dalam sistem file yang di-mount menggunakan FUSE.

1. `int res;`: Deklarasi variabel lokal res yang akan digunakan untuk menyimpan nilai hasil dari operasi rename.

2. `char fpath[1000];` dan `char fpath2[1000];`: Deklarasi array karakter fpath dan fpath2 dengan panjang 1000. Array ini akan digunakan untuk menyimpan path lengkap dari oldpath dan newpath dengan menambahkan homedir (direktori home) pada awalnya.

3. `sprintf(fpath, "%s%s", homedir, oldpath);` dan `sprintf(fpath2, "%s%s", homedir, newpath);`: Menggabungkan homedir dan oldpath ke dalam fpath, serta menggabungkan homedir dan newpath ke dalam fpath2 menggunakan sprintf. Ini bertujuan untuk mendapatkan path lengkap dari oldpath dan newpath yang akan digunakan dalam operasi rename.

4. `log_rename("REPORT", "RENAME", fpath, fpath2);`: Memanggil fungsi log_rename untuk mencatat pesan log dengan keterangan "RENAME" dan path lama (fpath) serta path baru (fpath2).

5. `res = rename(fpath, fpath2);`: Melakukan operasi rename dengan memindahkan file atau direktori dari fpath (path lama) ke fpath2 (path baru). Hasil dari operasi rename disimpan dalam variabel res.

6. `struct stat oldstat;`: Deklarasi struktur stat bernama oldstat yang akan digunakan untuk menyimpan informasi atribut dari fpath2 setelah dilakukan operasi rename.

7. `if (stat(fpath2, &oldstat) == -1) return -errno;`: Memeriksa apakah operasi stat pada fpath2 berhasil. Jika terjadi kesalahan, nilai -errno dikembalikan untuk menunjukkan jenis kesalahan yang terjadi.

8. `if (S_ISDIR(oldstat.st_mode))`: Memeriksa apakah fpath2 merupakan sebuah direktori dengan menggunakan S_ISDIR dan atribut st_mode dari oldstat. Jika benar, blok kode dalam kurung kurawal selanjutnya akan dijalankan.

9. `char \*result = strstr(newpath, "module*");`: Mencari substring "module\*" dalam newpath menggunakan fungsi strstr. Hasilnya disimpan dalam pointer result.

10. `if (result != NULL)`: Memeriksa apakah substring "module\_" ditemukan dalam newpath. Jika ditemukan, blok kode dalam kurung kurawal selanjutnya akan dijalankan.

11. `listFilesRecursively(fpath2, 0);`: Memanggil fungsi listFilesRecursively dengan parameter fpath2 dan 0. Fungsi ini akan secara rekursif mencetak daftar file dalam direktori fpath2 ke output standar dan flags 0 menandakan bahwa proses split file dilakukan.

12. `if (strstr(oldpath, "module*") != NULL && strstr(newpath, "module*") == NULL)`: Memeriksa apakah oldpath mengandung substring "module*" dan newpath tidak mengandung substring "module*". Jika kondisi ini terpenuhi, blok kode dalam kurung kurawal selanjutnya akan dijalankan.

13. `listFilesRecursively(fpath2, 1);`: Memanggil fungsi listFilesRecursively dengan parameter fpath2 dan 1. Fungsi ini akan secara rekursif menggabungkan file-file yang terpisah dalam direktori fpath2.

### 4.1.5 unlink

```c
static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    log_message("FLAG", "UNLINK", fpath);
    printf("unlink %s\n", fpath);
    int result = unlink(fpath);
    if (result == -1)
        return -errno;
    return 0;
}


```

Fungsi .unlink pada FUSE (Filesystem in Userspace) digunakan untuk menghapus (mengunlink) sebuah file dalam sistem file yang di-mount menggunakan FUSE.

Ketika fungsi .unlink dipanggil oleh sistem operasi, FUSE akan memanggil fungsi .unlink yang telah diimplementasikan dalam program FUSE yang sedang berjalan. Fungsi ini menerima satu parameter yaitu path yang menunjukkan path dari file yang akan dihapus.

1. `char fpath[1000];`: Deklarasi array karakter fpath dengan panjang 1000. Array ini akan digunakan untuk menyimpan path lengkap dari file yang akan dihapus.

2. `sprintf(fpath, "%s%s", homedir, path);`: Menggabungkan homedir (direktori home) dan path (path file yang akan dihapus) ke dalam fpath menggunakan sprintf. Tujuannya adalah untuk mendapatkan path lengkap dari file yang akan dihapus.

3. `log_message("FLAG", "UNLINK", fpath);`: Memanggil fungsi log_message dengan parameter "FLAG", "UNLINK", dan fpath. Fungsi ini digunakan untuk mencatat pesan log dengan keterangan "UNLINK" dan path file yang akan dihapus (fpath).

4. `int result = unlink(fpath);`: Melakukan operasi unlink untuk menghapus file yang ditentukan oleh fpath. Hasil dari operasi unlink disimpan dalam variabel result.

5. `if (result == -1) return -errno;`: Memeriksa apakah operasi unlink berhasil. Jika terjadi kesalahan, nilai -errno dikembalikan untuk menunjukkan jenis kesalahan yang terjadi.

### 4.1.6 rmdir

```c
static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("rmdir %s\n", fpath);
    log_message("FLAG", "RMDIR", fpath);
    int result = rmdir(fpath);
    if (result == -1)
        return -errno;
    return 0;
}


```

Fungsi .rmdir pada FUSE (Filesystem in Userspace) digunakan untuk menghapus (mengunlink) sebuah direktori kosong dalam sistem file yang di-mount menggunakan FUSE.

Ketika fungsi .rmdir dipanggil oleh sistem operasi, FUSE akan memanggil fungsi .rmdir yang telah diimplementasikan dalam program FUSE yang sedang berjalan. Fungsi ini menerima satu parameter yaitu path yang menunjukkan path dari direktori yang akan dihapus.

1. `char fpath[1000];`: Deklarasi array karakter fpath dengan panjang 1000. Array ini akan digunakan untuk menyimpan path lengkap dari direktori yang akan dihapus.

2. `sprintf(fpath, "%s%s", homedir, path);`: Menggabungkan homedir (direktori home) dan path (path direktori yang akan dihapus) ke dalam fpath menggunakan sprintf. Tujuannya adalah untuk mendapatkan path lengkap dari direktori yang akan dihapus.

3. `log_message("FLAG", "RMDIR", fpath);`: Memanggil fungsi log_message dengan parameter "FLAG", "RMDIR", dan fpath. Fungsi ini digunakan untuk mencatat pesan log dengan keterangan "RMDIR" dan path direktori yang akan dihapus (fpath).

4. `int result = rmdir(fpath);`: Melakukan operasi rmdir untuk menghapus direktori yang ditentukan oleh fpath. Hasil dari operasi rmdir disimpan dalam variabel result.

5. `if (result == -1) return -errno;`: Memeriksa apakah operasi rmdir berhasil. Jika terjadi kesalahan, nilai -errno dikembalikan untuk menunjukkan jenis kesalahan yang terjadi.

### 4.1.7 read

```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("read %s\n", fpath);
    log_message("REPORT", "READ", fpath);
    int fd;
    int res;

    (void)fi;
    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

```

Fungsi .read pada FUSE (Filesystem in Userspace) digunakan untuk membaca konten atau data dari sebuah file dalam sistem file yang di-mount menggunakan FUSE.

Ketika fungsi .read dipanggil oleh sistem operasi, FUSE akan memanggil fungsi .read yang telah diimplementasikan dalam program FUSE yang sedang berjalan. Fungsi ini menerima beberapa parameter, termasuk path yang menunjukkan path dari file yang akan dibaca, buf yang merupakan buffer tempat data akan disimpan, size yang menentukan jumlah maksimal byte yang akan dibaca, dan offset yang menentukan offset atau posisi dari mana pembacaan akan dimulai.

1. `char fpath[1000];`: Deklarasi array karakter fpath dengan panjang 1000. Array ini akan digunakan untuk menyimpan path lengkap dari file yang akan dibaca.

2. `sprintf(fpath, "%s%s", homedir, path);`: Menggabungkan homedir (direktori home) dan path (path file yang akan dibaca) ke dalam fpath menggunakan sprintf. Tujuannya adalah untuk mendapatkan path lengkap dari file yang akan dibaca.

3. `log_message("REPORT", "READ", fpath);`: Memanggil fungsi log_message dengan parameter "REPORT", "READ", dan fpath. Fungsi ini digunakan untuk mencatat pesan log dengan keterangan "READ" dan path file yang akan dibaca (fpath).

4. `(void)fi;`: Menghilangkan "unused parameter" warning dengan mengabaikan variabel fi yang tidak digunakan dalam implementasi ini.

5. `fd = open(fpath, O_RDONLY);`: Membuka file yang ditentukan oleh fpath dengan mode O_RDONLY (hanya membaca). Hasil dari operasi open disimpan dalam variabel fd.

6. `if (fd == -1) return -errno;`: Memeriksa apakah operasi open berhasil. Jika terjadi kesalahan, nilai -errno dikembalikan untuk menunjukkan jenis kesalahan yang terjadi.

7. `res = pread(fd, buf, size, offset);`: Membaca size byte data dari file yang telah dibuka ke dalam buffer buf, dimulai dari posisi offset. Hasil dari operasi pread disimpan dalam variabel res.

8. `if (res == -1) res = -errno;`: Memeriksa apakah operasi pread berhasil. Jika terjadi kesalahan, nilai -errno dikembalikan untuk menunjukkan jenis kesalahan yang terjadi.

9. `close(fd);`: Menutup file yang telah dibuka menggunakan open.

10. `return res;`: Mengembalikan nilai res yang merupakan hasil dari operasi .read. Jika operasi .read berhasil, nilai res akan berupa jumlah byte data yang berhasil dibaca. Jika terjadi kesalahan, nilai res akan berupa nilai negatif yang menunjukkan jenis kesalahan yang terjadi.

### 4.1.8 write

```c
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("write %s\n", fpath);
    log_message("REPORT", "WRITE", fpath);
    int fd;
    int res;

    (void)fi;
    fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}
```

Fungsi .write pada FUSE (Filesystem in Userspace) digunakan untuk menulis data ke dalam sebuah file dalam sistem file yang di-mount menggunakan FUSE.

Ketika fungsi .write dipanggil oleh sistem operasi, FUSE akan memanggil fungsi .write yang telah diimplementasikan dalam program FUSE yang sedang berjalan. Fungsi ini menerima beberapa parameter, termasuk path yang menunjukkan path dari file yang akan ditulis, buf yang berisi data yang akan ditulis, size yang menentukan jumlah byte data yang akan ditulis, dan offset yang menentukan posisi atau offset di dalam file tempat penulisan dimulai.

1. `char fpath[1000];`: Deklarasi array karakter fpath dengan panjang 1000. Array ini akan digunakan untuk menyimpan path lengkap dari file yang akan ditulis.

2. `sprintf(fpath, "%s%s", homedir, path);`: Menggabungkan homedir (direktori home) dan path (path file yang akan ditulis) ke dalam fpath menggunakan sprintf. Tujuannya adalah untuk mendapatkan path lengkap dari file yang akan ditulis.

3. `log_message("REPORT", "WRITE", fpath);`: Memanggil fungsi log_message dengan parameter "REPORT", "WRITE", dan fpath. Fungsi ini digunakan untuk mencatat pesan log dengan keterangan "WRITE" dan path file yang akan ditulis (fpath).

4. `(void)fi;`: Menghilangkan "unused parameter" warning dengan mengabaikan variabel fi yang tidak digunakan dalam implementasi ini.

5. `fd = open(fpath, O_WRONLY);`: Membuka file yang ditentukan oleh fpath dengan mode O_WRONLY (hanya menulis). Hasil dari operasi open disimpan dalam variabel fd.

6. `if (fd == -1) return -errno;`: Memeriksa apakah operasi open berhasil. Jika terjadi kesalahan, nilai -errno dikembalikan untuk menunjukkan jenis kesalahan yang terjadi.

7. `res = pwrite(fd, buf, size, offset);`: Menulis size byte data dari buffer buf ke dalam file yang telah dibuka, dimulai dari posisi offset. Hasil dari operasi pwrite disimpan dalam variabel res.

8. `if (res == -1) res = -errno;`: Memeriksa apakah operasi pwrite berhasil. Jika terjadi kesalahan, nilai -errno dikembalikan untuk menunjukkan jenis kesalahan yang terjadi.

9. `close(fd);`: Menutup file yang telah dibuka menggunakan open.

10. `return res;`: Mengembalikan nilai res yang merupakan hasil dari operasi .write. Jika operasi .write berhasil, nilai res akan berupa jumlah byte data yang berhasil ditulis. Jika terjadi kesalahan, nilai res akan berupa nilai negatif yang menunjukkan jenis kesalahan yang terjadi.

### 4.1.9 truncate

```c
static int xmp_truncate(const char *path, off_t size)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("truncate %s\n", fpath);
    log_message("REPORT", "TRUNCATE", fpath);
    int result = truncate(fpath, size);
    if (result == -1)
        return -errno;
    return 0;
}

```

Fungsi .truncate pada FUSE (Filesystem in Userspace) digunakan untuk mengubah ukuran (truncate) file yang ada dalam sistem file yang di-mount menggunakan FUSE. Truncate berarti mengubah ukuran file menjadi ukuran yang ditentukan.

Ketika fungsi .truncate dipanggil oleh sistem operasi, FUSE akan memanggil fungsi .truncate yang telah diimplementasikan dalam program FUSE yang sedang berjalan. Fungsi ini menerima beberapa parameter, termasuk path yang menunjukkan path dari file yang akan di-truncate (diubah ukurannya) dan size yang merupakan ukuran baru yang akan diberikan pada file tersebut.

### 4.1.10 create

```c
static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("create %s\n", fpath);
    log_message("REPORT", "CREATE", fpath);
    int result = creat(fpath, mode);
    if (result == -1)
        return -errno;
    fi->fh = result;
    return 0;
}
```

Fungsi .create pada FUSE (Filesystem in Userspace) digunakan untuk membuat file baru dalam sistem file yang di-mount menggunakan FUSE.

Ketika fungsi .create dipanggil oleh sistem operasi, FUSE akan memanggil fungsi .create yang telah diimplementasikan dalam program FUSE yang sedang berjalan. Fungsi ini menerima beberapa parameter, termasuk path yang menunjukkan path dari file yang akan dibuat, mode yang menentukan hak akses (permissions) dari file yang baru dibuat, dan fi yang merupakan struktur fuse_file_info yang berisi informasi tentang file yang baru dibuat.

### 4.1.11 mkdir

```c
static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("mkdir %s\n", fpath);
    log_message("REPORT", "MKDIR", fpath);
    int result = mkdir(fpath, mode);
    if (result == -1)
        return -errno;
    return 0;
}

```

Fungsi .create pada FUSE (Filesystem in Userspace) digunakan untuk membuat file baru dalam sistem file yang di-mount menggunakan FUSE.

Ketika fungsi .create dipanggil oleh sistem operasi, FUSE akan memanggil fungsi .create yang telah diimplementasikan dalam program FUSE yang sedang berjalan. Fungsi ini menerima beberapa parameter, termasuk path yang menunjukkan path dari file yang akan dibuat, mode yang menentukan hak akses (permissions) dari file yang baru dibuat, dan fi yang merupakan struktur fuse_file_info yang berisi informasi tentang file yang baru dibuat.

### 4.1.12 chmod

```c
static int xmp_chmod(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("chmod %s\n", fpath);
    log_message("REPORT", "CHMOD", fpath);
    int result = chmod(fpath, mode);
    if (result == -1)
        return -errno;
    return 0;
}
```

Fungsi .chmod pada FUSE (Filesystem in Userspace) digunakan untuk mengubah hak akses (permissions) dari file atau direktori dalam sistem file yang di-mount menggunakan FUSE.

Ketika fungsi .chmod dipanggil oleh sistem operasi, FUSE akan memanggil fungsi .chmod yang telah diimplementasikan dalam program FUSE yang sedang berjalan. Fungsi ini menerima beberapa parameter, termasuk path yang menunjukkan path dari file atau direktori yang hak aksesnya akan diubah, dan mode yang menentukan hak akses baru yang akan diberikan.

## 4.2 Modularisasi

### 4.2.1 List File Recursive

```c

void listFilesRecursively(char *basePath, int flags)
{
    char path[1000];
    struct dirent *dp;
    struct stat statbuf;

    char prevpath[4000];
    char currpath[4000];
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            printf("dp->d_name: %s\n", dp->d_name);
            snprintf(path, sizeof(path), "%s/%s", basePath, dp->d_name);

            if (stat(path, &statbuf) == 0)
            {
                if (S_ISDIR(statbuf.st_mode))
                {
                    listFilesRecursively(path, flags);
                }
                else
                {
                    if (!flags)
                    {
                        char command[5000];
                        splitFile(path);
                        sprintf(currpath, "%s", path);
                        sprintf(command, "rm %s", path);
                        system(command);
                        printf("%s\n", path);
                    }
                    else
                    {
                        char *substring;
                        int substring_length;
                        const char *dot = strchr(path, '.');
                        int length = strlen(path);
                        int dotCount = 0;
                        int i_loop;

                        for (i_loop = 0; i_loop < length; i_loop++)
                        {
                            if (path[i_loop] == '.')
                            {
                                dotCount++;
                                if (dotCount == 2)
                                {
                                    break;
                                }
                            }
                        }

                        if (dotCount == 2)
                        {
                            substring_length = (int)(dot - path);
                            substring = (char *)malloc(substring_length + 1);

                            strncpy(substring, path, i_loop);
                            substring[i_loop] = '\0';
                            sprintf(currpath, "%s", substring);
                            printf("substring yang diambil%s\n", substring);
                            free(substring);

                            if (strstr(prevpath, currpath) == NULL)
                            {
                                printf("memanggil fungsi concate separated string%s\n", currpath);
                                concateSeparatedFile(currpath);
                                dotCount = 0;
                            }
                        }
                        printf("\n");
                    }

                    sprintf(prevpath, "%s", currpath);
                }
            }
        }
    }
}

```

1. Fungsi listFilesRecursively memiliki parameter basePath yang merupakan path dari direktori yang akan ditelusuri dan flags yang menentukan perilaku tambahan.

2. Pertama, variabel path, prevpath, dan currpath dideklarasikan. path digunakan untuk menyimpan path lengkap dari setiap file yang ditemukan, prevpath digunakan untuk menyimpan path sebelumnya, dan currpath digunakan untuk menyimpan path saat ini.

3. Selanjutnya, variabel dp dan statbuf dideklarasikan untuk menyimpan informasi dirent (entry direktori) dan stat (informasi file/direktori) dari setiap entri dalam direktori.

4. Direktori dijalankan dengan menggunakan opendir untuk membuka direktori basePath. Jika direktori tidak dapat dibuka, maka fungsi keluar.

5. Selama membaca entri dalam direktori menggunakan readdir, dilakukan perulangan. Dalam perulangan ini, entri "." dan ".." diabaikan.

6. Dalam perulangan, path lengkap untuk setiap entri dihasilkan dengan menggabungkan basePath dan dp->d_name menggunakan snprintf.

7. Selanjutnya, menggunakan stat, informasi stat dari entri (file/direktori) tersebut diambil dan disimpan dalam statbuf.

8. Jika entri adalah sebuah direktori (diperiksa dengan S_ISDIR(statbuf.st_mode)), maka dilakukan rekursi dengan memanggil fungsi listFilesRecursively dengan path sebagai basePath untuk melanjutkan penelusuran rekursif ke dalam subdirektori tersebut.

9. Jika entri adalah sebuah file (tidak sebuah direktori), maka dilakukan beberapa logika tambahan tergantung pada nilai flags.

10. Jika flags adalah 0, maka dilakukan operasi penghapusan file menggunakan system("rm"). Sebelum menghapus file, dilakukan pemisahan file menggunakan fungsi splitFile dan path disimpan dalam currpath.

11. Jika flags bukan 0, maka dilakukan beberapa operasi tambahan. Misalnya, dilakukan pemrosesan untuk menghitung jumlah tanda titik dalam path dan mengambil substring dari path. Jika jumlah tanda titik adalah 2, maka dilakukan pengecekan dengan strstr untuk memastikan currpath tidak ada dalam prevpath. Jika tidak ada, dilakukan pemanggilan fungsi concateSeparatedFile untuk menggabungkan file-file terpisah.

12. Terakhir, prevpath diperbarui dengan currpath untuk digunakan dalam iterasi berikutnya.

### 4.2.2 Split File Modular

```c

void splitFile(const char *filename)
{
    FILE *file = fopen(filename, "rb");
    if (file == NULL)
    {
        perror("fopen");
        exit(1);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    int num_chunks = file_size / CHUNK_SIZE;
    if (file_size % CHUNK_SIZE)
    {
        num_chunks++;
    }

    char buffer[CHUNK_SIZE];
    size_t bytes_read;

    for (int i = 0; i < num_chunks; i++)
    {
        char output_filename[100];
        snprintf(output_filename, sizeof(output_filename), "%s.%03d", filename, i);

        FILE *output_file = fopen(output_filename, "wb");
        if (output_file == NULL)
        {
            fclose(file);
            perror("fopen");
            exit(1);
        }

        bytes_read = fread(buffer, 1, sizeof(buffer), file);
        fwrite(buffer, 1, bytes_read, output_file);
        fclose(output_file);
    }

    fclose(file);
    printf("Proses split file berhasi dilakukan menjadi %d\n", num_chunks);
}

```

1. Fungsi `splitFile` memiliki parameter filename yang merupakan nama file yang akan dibagi.

2. Pertama, file dengan nama filename dibuka dalam mode baca `("rb")` menggunakan fopen. Jika file tidak dapat dibuka, pesan kesalahan akan dicetak dan program akan keluar.

3. Menggunakan fseek, pointer file diatur pada posisi akhir file `(SEEK_END)` untuk mendapatkan ukuran file dengan `ftell`. Kemudian, pointer dikembalikan ke posisi awal file `(SEEK_SET)`.

4. Jumlah chunk dihitung berdasarkan ukuran file dibagi dengan `CHUNK_SIZE`. Jika ada sisa pembagian `(file_size % CHUNK_SIZE)`, maka jumlah chunk ditambah satu.

5. Array buffer dengan ukuran `CHUNK_SIZE` digunakan untuk membaca dan menulis data.

6. Dalam loop, setiap chunk dibaca dari file menggunakan `fread` dengan ukuran `CHUNK_SIZE`. Data yang dibaca disimpan dalam buffer.

7. Nama file untuk setiap chunk dibuat dengan menggunakan `snprintf` dengan menambahkan indeks pada nama file asli `(filename)` menggunakan format `"%s.%03d"`. Indeks diberikan sebagai angka tiga digit yang dimulai dari `000`.

8. File output dengan nama `output_filename` dibuka dalam mode tulis `("wb")` menggunakan `fopen`. Jika file tidak dapat dibuka, file input ditutup, pesan kesalahan dicetak, dan program keluar.

9. Data yang ada dalam buffer ditulis ke file output menggunakan `fwrite`.

10. Setelah selesai menulis data pada file output, file output ditutup.

11. Langkah-langkah 6-10 diulangi untuk setiap chunk hingga semua chunk selesai diproses.

12. Setelah semua chunk selesai diproses, file input ditutup.

13. Pesan berhasil dicetak yang menunjukkan jumlah chunk yang dihasilkan.

### 4.2.3 Concate Separated File

```c
static int concateSeparatedFile(char *filename)
{
    FILE *output_file;
    FILE *input_file;
    char buffer[1024];
    int part_number = 0;
    char part_filename[5000];
    char full_filename[4000];
    sprintf(full_filename, "%s", filename);
    output_file = fopen(full_filename, "wb");

    printf("filename: %s\n", full_filename);
    if (output_file == NULL)
    {
        printf("Gagal membuka file output.\n");
        return 1;
    }

    while (1)
    {
        sprintf(part_filename, "%s.%03d", full_filename, part_number);
        printf("part_filename: %s\n", part_filename);

        input_file = fopen(part_filename, "rb");
        if (input_file == NULL)
        {
            if (part_number == 0)
            {
                printf("Tidak ada file terpisah yang ditemukan.\n");
            }
            else
            {
                printf("Penyatuan file selesai. File utuh tersimpan dengan nama: %s\n", filename);
            }
            break;
        }

        while (fread(buffer, sizeof(char), sizeof(buffer), input_file) > 0)
        {
            fwrite(buffer, sizeof(char), sizeof(buffer), output_file);
        }

        fclose(input_file);

        char cmd[6000];
        sprintf(cmd, "rm %s", part_filename);
        system(cmd);
        remove(part_filename);

        part_number++;
    }

    fclose(output_file);

    return 0;
}
```

1. Fungsi `concateSeparatedFile` memiliki parameter filename yang merupakan nama file utuh yang akan disimpan setelah penggabungan.

2. Sebuah file output `(output_file)` dibuka dalam mode tulis `("wb")` dengan menggunakan fopen. Jika file tidak dapat dibuka, pesan kesalahan dicetak, dan fungsi mengembalikan nilai 1 sebagai indikasi kegagalan.

3. Dalam loop while, fungsi akan membaca dan menyatukan file-file terpisah yang memiliki format nama dengan indeks.

4. Nama file part dibangun dengan menggunakan sprintf dengan format `"%s.%03d"`, di mana full_filename adalah nama file utuh yang diambil dari parameter dan part_number adalah nomor indeks part yang akan digabungkan.

5. File part `(input_file)` dibuka dalam mode baca `("rb")` menggunakan fopen. Jika file tidak ditemukan, berarti semua file terpisah telah disatukan, dan pesan yang sesuai dicetak. Loop while dihentikan dengan menggunakan break.

6. Dalam loop while berikutnya, isi file part dibaca menggunakan `fread` dan ditulis ke file utuh menggunakan `fwrite`. Ukuran `buffer` yang digunakan adalah `sizeof(buffer)`.

7. Setelah selesai membaca dan menulis isi file part, file part yang telah digabungkan tidak lagi diperlukan. Oleh karena itu, file part ditutup menggunakan `fclose`, dan file part dihapus menggunakan remove atau dengan menggunakan perintah sistem menggunakan system dan rm.

8. Nomor indeks `part_number` ditingkatkan untuk memproses file part selanjutnya.

9. Setelah loop while selesai, file output `(output_file)` ditutup menggunakan `fclose`.

10. Fungsi mengembalikan nilai 0 sebagai indikasi keberhasilan.

### 4.2.4 Hasil

Folder dengan nama `inimodule` dengan isi file sebagai berikut sebelum diubah menjadi direktori modular

![Screenshot from 2023-06-03 03-40-06](https://github.com/dimss113/Grafkom-Mapping/assets/89715780/278931df-967a-4dca-bbd6-c6be4a4ef4cf)

folder dengan nama `inimodule` diubah menjadi nama `module_log` akan menjadi direktori modular dan file didalamnya akan dijadikan file modular serta file di dalam subdirektori `module_log` juga akan menjadi file modular

![Screenshot from 2023-06-03 03-41-47](https://github.com/dimss113/Grafkom-Mapping/assets/89715780/29a07dc5-e14f-4ef9-a89f-d71911fb7c10)

Apabila nama file diubah atau melakukan rename menjadi `bukanmodule`, maka yang semula modular akan menjadi seperti semula kembali.

![Screenshot from 2023-06-03 03-47-11](https://github.com/dimss113/Grafkom-Mapping/assets/89715780/37f5f349-5432-4263-bd08-4342ad835451)

## 4.3 Log Message

```c

static const char *log_file_path = "/home/dimasf/fs_module.log";

static void log_message(const char *level, const char *cmd, const char *desc)
{
    FILE *log_file = fopen(log_file_path, "a");
    if (log_file == NULL)
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    char *message_log = malloc(1000 * sizeof(char));

    time_t current_time = time(NULL);
    struct tm *tm = localtime(&current_time);

    int year = tm->tm_year % 100; // Mengambil 2 digit tahun (misal: 2021 menjadi 21)
    int month = tm->tm_mon + 1;   // Ditambah 1 karena indeks bulan dimulai dari 0
    int day = tm->tm_mday;
    int hour = tm->tm_hour;
    int minute = tm->tm_min;
    int second = tm->tm_sec;

    sprintf(message_log, "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s\n", level, year, month, day, hour, minute, second, cmd, desc);
    fprintf(log_file, "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s\n", level, year, month, day, hour, minute, second, cmd, desc);
    // printf("%s:%s:%s:%s\n", level, __DATE__, cmd, desc);
    fclose(log_file);
}
```

1. Fungsi log_message memiliki tiga parameter: level, cmd, dan desc, yang masing-masing mewakili tingkat log, perintah, dan deskripsi pesan log.

2. Sebuah file log (log_file) dibuka dalam mode tambah ("a") menggunakan fopen. Jika file tidak dapat dibuka, pesan kesalahan dicetak, dan program keluar dengan menggunakan exit(EXIT_FAILURE).

3. Sebuah string (message_log) dialokasikan dengan ukuran 1000 karakter menggunakan malloc. String ini akan digunakan untuk menyimpan pesan log yang akan dicetak ke file log.

4. Waktu saat ini diambil menggunakan time(NULL), dan struktur tm diinisialisasi menggunakan localtime(&current_time) untuk mendapatkan informasi waktu saat ini dalam bentuk yang lebih terperinci.

5. Komponen waktu seperti tahun, bulan, hari, jam, menit, dan detik diambil dari struktur tm menggunakan variabel-variabel year, month, day, hour, minute, dan second.

6. Pesan log dibuat menggunakan sprintf dengan format yang menggabungkan komponen-komponen waktu dan parameter level, cmd, dan desc. Pesan log ini akan dicetak ke file log menggunakan fprintf.

7. File log ditutup menggunakan fclose.

```c
static void log_rename(const char *level, const char *cmd, const char *oldpath, const char *newpath)
{
    printf("rename %s -> %s\n", oldpath, newpath);
    FILE *log_file = fopen(log_file_path, "a");
    if (log_file == NULL)
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    char *message_log = malloc(1000 * sizeof(char));

    time_t current_time = time(NULL);
    struct tm *tm = localtime(&current_time);

    int year = tm->tm_year % 100; // Mengambil 2 digit tahun (misal: 2021 menjadi 21)
    int month = tm->tm_mon + 1;   // Ditambah 1 karena indeks bulan dimulai dari 0
    int day = tm->tm_mday;
    int hour = tm->tm_hour;
    int minute = tm->tm_min;
    int second = tm->tm_sec;

    sprintf(message_log, "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s\n", level, year, month, day, hour, minute, second, cmd, oldpath, newpath);
    // printf("%s\n", message_log);
    fprintf(log_file, "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s\n", level, year, month, day, hour, minute, second, cmd, oldpath, newpath);
    fclose(log_file);
}


```

Fungsi yang sama dengan log_message hanya saja terdapat argument yang berbeda yaitu `oldpath` yang menyimpan nama file/folder sebelum di-rename dan `newpath` yang menyimpan nama/folder file setelah di-rename.

Hasil:

![Screenshot from 2023-06-03 03-54-16](https://github.com/dimss113/Grafkom-Mapping/assets/89715780/e811937f-f613-49a7-b933-d6cd7b48000d)


# Soal 5: FUSE dan Docker

## 5.1 Download dan Unzip File

Untuk mengunduh dan unzip file rahasia dapat dilakukan dengan fungsi `system()` sebagai berikut.

```c
system("wget 'https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes' -O rahasia.zip");
system("unzip rahasia.zip");
```

Selain itu, perlu dilakukan pembuatan folder dan juga file yang akan dibutuhkan pada pengerjaan subsoal-subsoal lain sebagai berikut.

```c
const char *dirpath = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/rahasia";
const char *user_dir = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/user";
const char *users_file = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/user/data.txt";
const char *logged_in_file = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/user/logged_in.txt";
const char *result_file = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/result.txt";
const char *extension_file = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/extension.txt";

void init() {
    if (access(user_dir, F_OK) == 0) return;
    mkdir(user_dir, 0777);

    char create_file[1000];
    sprintf(create_file, "touch '%s'", users_file);
    system(create_file);
    
    sprintf(create_file, "touch '%s'", logged_in_file);
    system(create_file);

    if (access(result_file, F_OK) == 0) return;
    sprintf(create_file, "touch '%s'", result_file);
    system(create_file);

    if (access(extension_file, F_OK) == 0) return;
    sprintf(create_file, "touch '%s'", extension_file);
    system(create_file);

    system("wget 'https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes' -O rahasia.zip");
    system("unzip rahasia.zip");
}
```

Berikut adalah hasil inisiasi workspace dari soal nomor 5.

![Screenshot from 2023-06-03 23-16-12](https://github.com/mvinorian/Modul1_Probstat_5025211045/assets/54766683/3d1b6d1b-001a-429d-8f85-1c671bd7041c)


## 5.2 Mount FUSE rahasia di /usr/share/rahasia

Untuk melakukan mount folder rahasia pada `/usr/share/rahasia` dapat dilakukan dengan fungsi-fungsi sebagai berikut.

```c
static int xmp_getattr(const char *path, struct stat *stbuf) {
    char fpath[1000];
    sprintf(fpath,"%s%s",dirpath,path);

    int res;
    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);
    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0) break;
    }

    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    if (strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0 ;
    (void) fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    return res;
}

static int xmp_rename(const char* from, const char* to) {
    char oldpath[1000];
    char newpath[1000];

    sprintf(oldpath, "%s%s", dirpath, from);
    sprintf(newpath, "%s%s", dirpath, to);

    int res = rename(oldpath, newpath);
    if (res == -1) res = -errno;

    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};
```

Operasi FUSE yang ditambahkan adalah `.getattr` agar folder mount dapat menampilkan attribut dari entry folder, `.readdir` agar folder mount dapat membaca folder, `.read` agar folder mount dapat membaca file, dan juga `.rename` agar entry folder mount dapat direname.

Kemudian untuk melakukan mount `/usr/share/rahasia` pada lokal ke docker container, dapat dibuat docker compose sebagai berikut.

```yml
version: '3'
services:
  rahasia_di_docker_c02:
    image: ubuntu:focal
    volumes:
      - /usr/share/rahasia:/usr/share/rahasia
    working_dir: /usr/share/rahasia
    command: /bin/bash
    cap_add:
      - SYS_ADMIN
    devices:
      - /dev/fuse:/dev/fuse
    security_opt:
      - apparmor:unconfined
    tty: true
```

Service dari docker compose akan dinamakan dengan `rahasia_di_docker_c02` yang akan membuat container dengan base image `ubuntu:focal`. Kemudian perlu didefinisikan `volumes` agar menyambungkan `/usr/share/rahasia` yang berada pada host dengan `/usr/share/rahasia` pada container. Setelah itu, `working_dir` akan diganti ke `/usr/share/rahasia` agar ketika container tersebut dijalankan akan langsung berpindah ke directory `/usr/share/rahasia`. Kemudian ketika container dijalankan akan menjalankan command `/bin/bash`. Setelah itu, agar docker container dapat membaca folder mount hasil fuse, maka perlu ditambahkan opsi keamanan docker container agar berjalan sebagai administrator dengan parameter `cap_add` dan `security_opt`. Container juga perlu untuk mengakses device dari fuse pada host, sehingga ditambahkan `devices` untuk menghubungkan `/dev/fuse` pada host dan juga container.

Program `rahasia.c` dapat dicompile dengan perintah sebagai berikut.

```bash
gcc -Wall `pkg-config fuse --cflags` -g rahasia.c -o rahasia.app `pkg-config fuse --libs`
```

Kemudian dapat dijalankan dengan perintah `rahasia.app /usr/share/rahasia`. Berikut adalah hasil mount folder rahasia pada host dan juga container.

![Screenshot from 2023-06-03 23-39-06](https://github.com/mvinorian/Modul1_Probstat_5025211045/assets/54766683/07fb0d55-6796-4555-a1cf-05c004a08801)

Terlihat bahwa folder `/usr/share/rahasia` berhasil mount dari folder rahasia pada host (kiri) dan juga container (kanan).

## 5.3 Sistem Register

Untuk membuat sistem register perlu dibuat sebuah file yang digunakan untuk menyimpan data user dan password `./user/data.txt` serta untuk menyimpan user yang sedang login `./user/logged_in.txt`. Kedua file tersebut telah dibuat pada proses inisiasi workspace.

Kemudian dapat dilakukan pengecekan apakah terdapat user yang login dengan fungsi berikut.

```c
static int logged_in() {
    char verify[1000];
    sprintf(verify, "[ -s '%s' ]", logged_in_file);
    return system(verify);
}
```

Dengan memanfaatkan fungsi tersebut, operasi fuse dapat dimodifikasi pada operasi `.readdir`, `.read`, dan juga `.rename` dengan melakukan pengecekan apakah terdapat user yang login atau tidak. Jika tidak ada user yang login maka akan dikembalikan nilai error sehingga folder mount tidak dapat mengakses entry.

Untuk melakukan login, perlu dicek apakah kredensial yang dimasukkan terdapat pada data user atau belum dengan menggunakan fungsi berikut.

```c
int menu_login() {
    char username[100];
    char password[100];
    char find_account[250];
    char hash_password[250];

    printf("\n[Menu Login]\n");
    printf("Masukkan username: ");
    scanf(" %[^\n]", username);
    printf("Masukkan password: ");
    scanf(" %[^\n]", password);

    sprintf(hash_password, "echo -n '%s' | md5sum", password);

    FILE *password_hash;
    password_hash = popen(hash_password, "r");
    if (password_hash) fscanf(password_hash, "%s", password);

    sprintf(find_account, "grep -wq '%s;%s' '%s'", username, password, users_file);
    if(system(find_account) == 0) {
        if (logged_in() == 0) {
            printf("Anda masih login dengan akun lain. Silahkan logout terlebih dahulu!\n");
            return -1;
        }

        char login[250];
        sprintf(login, "echo '%s' > '%s'", username, logged_in_file);
        system(login);

        printf("Login berhasil\n");
        return 0;
    }

    printf("Akun belum terdaftar atau kata sandi salah\n");
    return -1;
}
```

Proses login dilakukan dengan menggunakan perintah bash sederhana yaitu `grep` dan juga `md5sum` untuk melakukan enkripsi password.

Kemudian untuk proses register, perlu dilakukan pengecekan apakah username telah terdaftar atau belum, jika belum akan ditambahkan pada data user dengan menggunakan fungsi berikut.

```c
int menu_register() {
    char username[100];
    char password[100];
    char find_username[250];
    char hash_password[250];
    char write_account[250];

    printf("\n[Menu Register]\n");
    printf("Masukkan username: ");
    scanf(" %[^\n]", username);
    printf("Masukkan password: ");
    scanf(" %[^\n]", password);

    sprintf(hash_password, "echo -n '%s' | md5sum", password);

    FILE *password_hash;
    password_hash = popen(hash_password, "r");
    if (password_hash) fscanf(password_hash, "%s", password);
    
    sprintf(find_username, "grep -oq '%s;' '%s'", username, users_file);
    if (system(find_username) == 0) {
        printf("Username telah terdaftar\n");
        return -1;
    }

    sprintf(write_account, "echo '%s;%s' >> '%s'", username, password, users_file);
    system(write_account);
    printf("Register berhasil\n");
    return 0;
}
```

Mirip seperti fungsi login, fungsi register juga menggunakan perintah bash sederhana yaitu `grep` dan `md5sum`.

Kemudian untuk menyatukan keduanya, perlu dibuat fungsi menu dan juga ditambahkan agumen pada program `rahasia.app` agar ketika dijalankan dapat menampilkan menu dan tidak melakukan FUSE mount sebagai berikut.

```c
void menu(char* src) {
    int command;

    while (1) {
        printf("\n[Menu]\n");
        printf("1. Login\n");
        printf("2. Register\n");
        printf("3. Logout\n");
        printf("4. Exit\n");
        printf("Masukkan pilihan: ");
        scanf("%d", &command);
        
        switch (command) {
            case 1: {
                menu_login();
                break;
            }
            case 2: {
                menu_register();
                break;
            }
            case 3: {
                menu_logout();
                break;
            }
            case 4: {
                return;
            }
            default: menu_login();
        }
    }
}

int main(int argc, char* argv[]) {
    init();
    if (argc > 2 && strcmp(argv[1], "-m") == 0) menu(argv[2]);
    else {
        umask(0);
        return fuse_main(argc, argv, &xmp_oper, NULL);
    }

    return 0;
}
```

Berikut adalah demonstrasi login dan register beserta akses folder mount.

* Belum terdapat user yang register maupun login
![Screenshot from 2023-06-03 23-54-45](https://github.com/mvinorian/Modul1_Probstat_5025211045/assets/54766683/face08b0-4b84-4be8-8586-2d87ad2c0e2c)

* Terdapat user yang login
![Screenshot from 2023-06-03 23-57-27](https://github.com/mvinorian/Modul1_Probstat_5025211045/assets/54766683/b3c9f992-c6ea-44b7-b19f-d9a23979614a)

## 5.4 Rename Folder dan File serta Pengerjaan Subsoal E

Untuk melakukan perubahan nama pada entry folder mount dapat digunakan fungsi berikut.

```c
void rename_all(char* src) {
    if (logged_in() != 0) return;

    DIR* src_dir;
    struct dirent* src_ent;
    src_dir = opendir(src);

    while ((src_ent = readdir(src_dir)) != NULL) {
        if (
            strcmp(src_ent->d_name, ".") == 0 ||
            strcmp(src_ent->d_name, "..") == 0
        ) continue;

        if (src_ent->d_type == DT_DIR) {
            char old_src[1000];
            sprintf(old_src, "%s/%s", src, src_ent->d_name);

            char new_src[1000];
            sprintf(new_src, "%s/%s_C02", src, src_ent->d_name);

            rename(old_src, new_src);
            rename_all(new_src);
        }

        else if (src_ent->d_type == DT_REG) {
            char old_name[1000];
            sprintf(old_name, "%s/%s", src, src_ent->d_name);

            char new_name[1000];
            sprintf(new_name, "%s/C02_%s", src, src_ent->d_name);

            rename(old_name, new_name);
        }
    }
}
```

Fungsi tersebut akan dijalankan secara rekursif, sehingga ketika terdapat subfolder, semua entry di dalamnya akan direname juga. Rename dilakukan pada folder yang telah dimount bukan pada folder rahasia.

Kemudian untuk menyelesaikan subsoal E dapat dilakukan dengan fungsi-fungsi berikut.

```c
char* get_ext(char* filename) {
    char* ext = strrchr(filename, '.');
    if (ext == NULL) return "";
    return ext + 1;
}

void write_extension(char* base_src, char* src) {
    if (logged_in() != 0) return;

    DIR* src_dir;
    struct dirent* src_ent;
    src_dir = opendir(src);

    while ((src_ent = readdir(src_dir)) != NULL) {
        if (
            strcmp(src_ent->d_name, ".") == 0 ||
            strcmp(src_ent->d_name, "..") == 0
        ) continue;

        if (src_ent->d_type == DT_DIR) {
            char new_src[1000];
            sprintf(new_src, "%s/%s", src, src_ent->d_name);
            write_extension(base_src, new_src);
        }

        else if (src_ent->d_type == DT_REG) {
            char ext[10];
            strcpy(ext, get_ext(src_ent->d_name));
            
            char find_ext[100];
            sprintf(find_ext, "grep -oq '%s' '%s'", ext, extension_file);
            if (system(find_ext) != 0) {
                char write_ext[250];
                sprintf(write_ext, "echo %s = $(find '%s' -type f -name '*.%s' | wc -l) >> '%s'", ext, base_src, ext, extension_file);
                system(write_ext);
            }
        }
    }
}

void solve_e(char* src) {
    char write_result[250];
    sprintf(write_result, "tree '%s' > '%s'", src, result_file);
    system(write_result);

    char write_folder[250];
    sprintf(write_folder, "echo folder = $(find '%s' -type d | wc -l) > '%s'", src, extension_file);
    system(write_folder);

    write_extension(src, src);
}
```

Dengan menggunakan perintah bash `tree` dapat digunakan untuk mengerjakan file `result.txt` dan juga untuk perhitungan ekstensi file dapat dilakukan menggunakan logika yang sama dengan fungsi `rename_all` yaitu secara rekursif, sehingga semua file dengan ekestensi tertentu akan dicatat menggunakan perintah bash `find`. Ketika jumlah ekstensi file tersebut sudah dicatat maka akan dilewati untuk file tersebut. Berikut adalah hasil dari rename dan juga subsoal E.

![Screenshot from 2023-06-04 00-06-32](https://github.com/mvinorian/Modul1_Probstat_5025211045/assets/54766683/7e869998-a11e-4d1c-b7a1-53edaaae3be9)


![Screenshot from 2023-06-04 00-07-18](https://github.com/mvinorian/Modul1_Probstat_5025211045/assets/54766683/495b3ede-9f8a-4124-baa3-5e740e920a3d)

![Screenshot from 2023-06-04 00-07-24](https://github.com/mvinorian/Modul1_Probstat_5025211045/assets/54766683/7eb5215e-17dc-49d9-a003-64f7816f3414)

