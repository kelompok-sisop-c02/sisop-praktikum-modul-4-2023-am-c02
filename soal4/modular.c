

#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <pwd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include <limits.h>
#include <fcntl.h>
#include <sys/statvfs.h>
#include <sys/time.h>
#include <time.h>
#include <grp.h>
#include <sys/xattr.h>
#define CHUNK_SIZE 1024

static const char *homedir = "/home/dimasf";
static const char *log_file_path = "/home/dimasf/fs_module.log";

// LEVEL:YYMMDD-HHMMSS:COMMAND:DESC

static int concateSeparatedFile(char *filename)
{
    FILE *output_file;
    FILE *input_file;
    char buffer[1024];
    int part_number = 0;
    char part_filename[5000];
    char full_filename[4000];
    // sprintf(full_filename, "%s%s", filename, ".txt");
    sprintf(full_filename, "%s", filename);
    // Membuka file output untuk menyimpan file utuh
    output_file = fopen(full_filename, "wb");

    printf("filename: %s\n", full_filename);
    if (output_file == NULL)
    {
        printf("Gagal membuka file output.\n");
        return 1;
    }

    // Membaca dan menyatukan file-file yang terpisah
    while (1)
    {
        // Membangun nama file part
        sprintf(part_filename, "%s.%03d", full_filename, part_number);
        printf("part_filename: %s\n", part_filename);

        // Membuka file part untuk dibaca
        input_file = fopen(part_filename, "rb");
        if (input_file == NULL)
        {
            // Jika file part tidak ditemukan, berarti semua file terpisah telah disatukan
            if (part_number == 0)
            {
                printf("Tidak ada file terpisah yang ditemukan.\n");
            }
            else
            {
                printf("Penyatuan file selesai. File utuh tersimpan dengan nama: %s\n", filename);
            }
            break;
        }

        // Membaca isi file part dan menulisnya ke file utuh
        while (fread(buffer, sizeof(char), sizeof(buffer), input_file) > 0)
        {
            fwrite(buffer, sizeof(char), sizeof(buffer), output_file);
        }

        // Menutup file part yang telah dibaca
        fclose(input_file);

        // Menghapus file part yang telah disatukan
        char cmd[6000];
        sprintf(cmd, "rm %s", part_filename);
        system(cmd);
        remove(part_filename);

        part_number++;
    }

    // Menutup file output
    fclose(output_file);

    return 0;
}

void splitFile(const char *filename)
{
    FILE *file = fopen(filename, "rb");
    if (file == NULL)
    {
        perror("fopen");
        exit(1);
    }

    fseek(file, 0, SEEK_END);     // digunakan untuk menentukan posisi pointer pada akhir file
    long file_size = ftell(file); // digunakan untuk menentukan posisi pointer
    fseek(file, 0, SEEK_SET);     // digunakan untuk menentukan posisi pointer pada awal file

    int num_chunks = file_size / CHUNK_SIZE;
    if (file_size % CHUNK_SIZE)
    {
        num_chunks++;
    }

    char buffer[CHUNK_SIZE];
    size_t bytes_read;

    for (int i = 0; i < num_chunks; i++)
    {
        char output_filename[100];
        snprintf(output_filename, sizeof(output_filename), "%s.%03d", filename, i);

        FILE *output_file = fopen(output_filename, "wb");
        if (output_file == NULL)
        {
            fclose(file);
            perror("fopen");
            exit(1);
        }

        bytes_read = fread(buffer, 1, sizeof(buffer), file);
        fwrite(buffer, 1, bytes_read, output_file);
        fclose(output_file);
    }

    fclose(file);
    printf("Proses split file berhasi dilakukan menjadi %d\n", num_chunks);
}

void listFilesRecursively(char *basePath, int flags)
{
    char path[1000];
    struct dirent *dp;
    struct stat statbuf;

    char prevpath[4000];
    char currpath[4000];

    // char prev_dot;

    DIR *dir = opendir(basePath);

    // mengecek apakah direktori berhasil dibuka
    if (!dir)
        return;

    // membaca semua entri dalam direktori
    while ((dp = readdir(dir)) != NULL)
    {
        // mengabaikan entri "." dan ".."
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            printf("dp->d_name: %s\n", dp->d_name);
            snprintf(path, sizeof(path), "%s/%s", basePath, dp->d_name);

            // mengambil informasi stat dari entri
            if (stat(path, &statbuf) == 0)
            {
                // jika entri adalah direktori, maka rekursif
                if (S_ISDIR(statbuf.st_mode))
                {
                    listFilesRecursively(path, flags);
                }
                else
                {
                    if (!flags)
                    {
                        char command[5000];
                        splitFile(path);
                        sprintf(currpath, "%s", path);
                        sprintf(command, "rm %s", path);
                        system(command);
                        printf("%s\n", path);
                    }
                    else
                    {
                        char *substring;
                        int substring_length;
                        const char *dot = strchr(path, '.');
                        int length = strlen(path);
                        int dotCount = 0;
                        int i_loop;

                        for (i_loop = 0; i_loop < length; i_loop++)
                        {
                            if (path[i_loop] == '.')
                            {
                                dotCount++;
                                if (dotCount == 2)
                                {
                                    break;
                                }
                            }
                        }

                        if (dotCount == 2)
                        {
                            substring_length = (int)(dot - path);
                            substring = (char *)malloc(substring_length + 1);

                            strncpy(substring, path, i_loop);
                            substring[i_loop] = '\0';
                            sprintf(currpath, "%s", substring);
                            printf("substring yang diambil%s\n", substring);
                            free(substring);

                            if (strstr(prevpath, currpath) == NULL)
                            {
                                printf("memanggil fungsi concate separated string%s\n", currpath);
                                concateSeparatedFile(currpath);
                                dotCount = 0;
                            }
                        }
                        printf("\n");
                    }

                    sprintf(prevpath, "%s", currpath);
                }
            }
        }
    }
}

static void log_message(const char *level, const char *cmd, const char *desc)
{
    FILE *log_file = fopen(log_file_path, "a");
    if (log_file == NULL)
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    char *message_log = malloc(1000 * sizeof(char));

    time_t current_time = time(NULL);
    struct tm *tm = localtime(&current_time);

    int year = tm->tm_year % 100; // Mengambil 2 digit tahun (misal: 2021 menjadi 21)
    int month = tm->tm_mon + 1;   // Ditambah 1 karena indeks bulan dimulai dari 0
    int day = tm->tm_mday;
    int hour = tm->tm_hour;
    int minute = tm->tm_min;
    int second = tm->tm_sec;

    sprintf(message_log, "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s\n", level, year, month, day, hour, minute, second, cmd, desc);
    fprintf(log_file, "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s\n", level, year, month, day, hour, minute, second, cmd, desc);
    // printf("%s:%s:%s:%s\n", level, __DATE__, cmd, desc);
    fclose(log_file);
}

static void log_rename(const char *level, const char *cmd, const char *oldpath, const char *newpath)
{
    printf("rename %s -> %s\n", oldpath, newpath);
    FILE *log_file = fopen(log_file_path, "a");
    if (log_file == NULL)
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    char *message_log = malloc(1000 * sizeof(char));

    time_t current_time = time(NULL);
    struct tm *tm = localtime(&current_time);

    int year = tm->tm_year % 100; // Mengambil 2 digit tahun (misal: 2021 menjadi 21)
    int month = tm->tm_mon + 1;   // Ditambah 1 karena indeks bulan dimulai dari 0
    int day = tm->tm_mday;
    int hour = tm->tm_hour;
    int minute = tm->tm_min;
    int second = tm->tm_sec;

    sprintf(message_log, "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s\n", level, year, month, day, hour, minute, second, cmd, oldpath, newpath);
    // printf("%s\n", message_log);
    fprintf(log_file, "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s\n", level, year, month, day, hour, minute, second, cmd, oldpath, newpath);
    fclose(log_file);
}

static int xmp_rename(const char *oldpath, const char *newpath)
{
    printf("rename %s -> %s\n", oldpath, newpath);

    int res;
    char fpath[1000];
    char fpath2[1000];
    sprintf(fpath, "%s%s", homedir, oldpath);
    sprintf(fpath2, "%s%s", homedir, newpath);
    log_rename("REPORT", "RENAME", fpath, fpath2);
    printf("rename %s -> %s\n", fpath, fpath2);

    res = rename(fpath, fpath2);
    if (res == -1)
        return -errno;

    struct stat oldstat;

    if (stat(fpath2, &oldstat) == -1)
        return -errno;

    if (S_ISDIR(oldstat.st_mode))
    {
        char *result = strstr(newpath, "module_");
        // char *result2 = strstr(oldpath, "module_");
        if (result != NULL)
        {
            listFilesRecursively(fpath2, 0);
        }
        if (strstr(oldpath, "module_") != NULL && strstr(newpath, "module_") == NULL)
        {
            // char command[5000];
            // sprintf(command, "mkdir %s", fpath2);
            // system(command);
            printf("Concate separated file\n");
            listFilesRecursively(fpath2, 1);
        }

        printf("renaming directory\n");
    }
    else
    {
        printf("renaming file\n");
    }

    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;
    char fpath[1000];

    strcpy(fpath, homedir);
    strncat(fpath, path, sizeof(fpath) - strlen(fpath) - 1);
    // log_message("REPORT", "GETATTR", fpath);

    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    char fpath[1000];
    static int readCount = 0;
    char newpath[2000];

    (void)offset;
    (void)fi;

    strcpy(fpath, homedir);
    strncat(fpath, path, sizeof(fpath) - strlen(fpath) - 1);
    log_message("REPORT", "READDIR", fpath);
    // printf("readdir %s\n", fpath);

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        // printf("name %s\n", de->d_name);
        sprintf(newpath, "%s%s", fpath, de->d_name);
        if (strstr(newpath, "module_") && !readCount)
        {
            printf("newpath readdir %s\n", newpath);
            listFilesRecursively(newpath, 0);
            readCount++;
            printf("readCount %d", readCount);
        }
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    int res = 0;
    char fpath[1000];

    strcpy(fpath, homedir);
    strncat(fpath, path, sizeof(fpath) - strlen(fpath) - 1);
    log_message("REPORT", "OPEN", fpath);
    printf("open %s\n", fpath);

    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;

    fi->fh = res;

    close(res);
    return 0;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    log_message("FLAG", "UNLINK", fpath);
    printf("unlink %s\n", fpath);
    int result = unlink(fpath);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("rmdir %s\n", fpath);
    log_message("FLAG", "RMDIR", fpath);
    int result = rmdir(fpath);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("read %s\n", fpath);
    log_message("REPORT", "READ", fpath);
    int fd;
    int res;

    (void)fi;
    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("write %s\n", fpath);
    log_message("REPORT", "WRITE", fpath);
    int fd;
    int res;

    (void)fi;
    fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int xmp_truncate(const char *path, off_t size)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("truncate %s\n", fpath);
    log_message("REPORT", "TRUNCATE", fpath);
    int result = truncate(fpath, size);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("create %s\n", fpath);
    log_message("REPORT", "CREATE", fpath);
    int result = creat(fpath, mode);
    if (result == -1)
        return -errno;
    fi->fh = result;
    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("mkdir %s\n", fpath);
    log_message("REPORT", "MKDIR", fpath);
    int result = mkdir(fpath, mode);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_chmod(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("chmod %s\n", fpath);
    log_message("REPORT", "CHMOD", fpath);
    int result = chmod(fpath, mode);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("chown %s\n", fpath);
    log_message("REPORT", "CHOWN", fpath);
    int result = lchown(fpath, uid, gid);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_utimens(const char *path, const struct timespec ts[2])
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("utimens %s\n", fpath);
    int result = utimensat(0, fpath, ts, AT_SYMLINK_NOFOLLOW);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_access(const char *path, int mask)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("access %s\n", fpath);
    int result = access(fpath, mask);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_release(const char *path, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    // printf("release %s\n", fpath);
    close(fi->fh);
    return 0;
}

static int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("fsync %s\n", fpath);
    int result;
    if (isdatasync)
        result = fdatasync(fi->fh);
    else
        result = fsync(fi->fh);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_symlink(const char *from, const char *to)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, from);
    printf("symlink %s\n", fpath);
    int result = symlink(fpath, to);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_link(const char *from, const char *to)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, from);
    printf("link %s\n", fpath);
    int result = link(fpath, to);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_statfs(const char *path, struct statvfs *stbuf)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("statfs %s\n", fpath);
    int result = statvfs(fpath, stbuf);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("setxattr %s\n", fpath);
    int result = lsetxattr(fpath, name, value, size, flags);
    if (result == -1)
        return -errno;
    return 0;
}

static int xmp_getxattr(const char *path, const char *name, char *value, size_t size)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("getxattr %s\n", fpath);
    int result = lgetxattr(fpath, name, value, size);
    if (result == -1)
        return -errno;
    return result;
}

static int xmp_listxattr(const char *path, char *list, size_t size)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("listxattr %s\n", fpath);
    int result = llistxattr(fpath, list, size);
    if (result == -1)
        return -errno;
    return result;
}

static int xmp_removexattr(const char *path, const char *name)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", homedir, path);
    printf("removexattr %s\n", fpath);
    int result = lremovexattr(fpath, name);
    if (result == -1)
        return -errno;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .open = xmp_open,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .read = xmp_read,
    .write = xmp_write,
    .truncate = xmp_truncate,
    .create = xmp_create,
    .mkdir = xmp_mkdir,
    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .utimens = xmp_utimens,
    .access = xmp_access,
    .release = xmp_release,
    .fsync = xmp_fsync,
    .symlink = xmp_symlink,
    .link = xmp_link,
    .statfs = xmp_statfs,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
    .removexattr = xmp_removexattr,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
