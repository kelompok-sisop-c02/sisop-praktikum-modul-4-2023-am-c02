#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main() {
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip");
    system("awk -F',' '{ if ( $3<25 && $8>85 && $9!=\"Manchester City\" ) print $2 \" - \" $9 \" - \" $3 \" - \" $8 \" - \" $4 \" - \" $14}' FIFA23_official_data.csv");
}
