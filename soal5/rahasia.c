#define _GNU_SOURCE
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>

const char *dirpath = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/rahasia";
const char *user_dir = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/user";
const char *users_file = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/user/data.txt";
const char *logged_in_file = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/user/logged_in.txt";
const char *result_file = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/result.txt";
const char *extension_file = "/home/mvinorian/sisop/praktikum/praktikum-4/soal5/extension.txt";

void init() {
    if (access(user_dir, F_OK) == 0) return;
    mkdir(user_dir, 0777);

    char create_file[1000];
    sprintf(create_file, "touch '%s'", users_file);
    system(create_file);
    
    sprintf(create_file, "touch '%s'", logged_in_file);
    system(create_file);

    if (access(result_file, F_OK) == 0) return;
    sprintf(create_file, "touch '%s'", result_file);
    system(create_file);

    if (access(extension_file, F_OK) == 0) return;
    sprintf(create_file, "touch '%s'", extension_file);
    system(create_file);

    system("wget 'https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=yes' -O rahasia.zip");
    system("unzip rahasia.zip");
}

static int logged_in() {
    char verify[1000];
    sprintf(verify, "[ -s '%s' ]", logged_in_file);
    return system(verify);
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
    char fpath[1000];
    sprintf(fpath,"%s%s",dirpath,path);

    int res;
    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    if (logged_in() != 0) return -errno;

    char fpath[1000];

    if (strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);
    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0) break;
    }

    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    if (logged_in() != 0) return -errno;

    char fpath[1000];
    if (strcmp(path,"/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0 ;
    (void) fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    return res;
}

static int xmp_rename(const char* from, const char* to) {
    if (logged_in() != 0) return -errno;

    char oldpath[1000];
    char newpath[1000];

    sprintf(oldpath, "%s%s", dirpath, from);
    sprintf(newpath, "%s%s", dirpath, to);

    int res = rename(oldpath, newpath);
    if (res == -1) res = -errno;

    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};

void rename_all(char* src) {
    if (logged_in() != 0) return;

    DIR* src_dir;
    struct dirent* src_ent;
    src_dir = opendir(src);

    while ((src_ent = readdir(src_dir)) != NULL) {
        if (
            strcmp(src_ent->d_name, ".") == 0 ||
            strcmp(src_ent->d_name, "..") == 0
        ) continue;

        if (src_ent->d_type == DT_DIR) {
            char old_src[1000];
            sprintf(old_src, "%s/%s", src, src_ent->d_name);

            char new_src[1000];
            sprintf(new_src, "%s/%s_C02", src, src_ent->d_name);

            rename(old_src, new_src);
            rename_all(new_src);
        }

        else if (src_ent->d_type == DT_REG) {
            char old_name[1000];
            sprintf(old_name, "%s/%s", src, src_ent->d_name);

            char new_name[1000];
            sprintf(new_name, "%s/C02_%s", src, src_ent->d_name);

            rename(old_name, new_name);
        }
    }
}

char* get_ext(char* filename) {
    char* ext = strrchr(filename, '.');
    if (ext == NULL) return "";
    return ext + 1;
}

void write_extension(char* base_src, char* src) {
    if (logged_in() != 0) return;

    DIR* src_dir;
    struct dirent* src_ent;
    src_dir = opendir(src);

    while ((src_ent = readdir(src_dir)) != NULL) {
        if (
            strcmp(src_ent->d_name, ".") == 0 ||
            strcmp(src_ent->d_name, "..") == 0
        ) continue;

        if (src_ent->d_type == DT_DIR) {
            char new_src[1000];
            sprintf(new_src, "%s/%s", src, src_ent->d_name);
            write_extension(base_src, new_src);
        }

        else if (src_ent->d_type == DT_REG) {
            char ext[10];
            strcpy(ext, get_ext(src_ent->d_name));
            
            char find_ext[100];
            sprintf(find_ext, "grep -oq '%s' '%s'", ext, extension_file);
            if (system(find_ext) != 0) {
                char write_ext[250];
                sprintf(write_ext, "echo %s = $(find '%s' -type f -name '*.%s' | wc -l) >> '%s'", ext, base_src, ext, extension_file);
                system(write_ext);
            }
        }
    }
}

void solve_e(char* src) {
    char write_result[250];
    sprintf(write_result, "tree '%s' > '%s'", src, result_file);
    system(write_result);

    char write_folder[250];
    sprintf(write_folder, "echo folder = $(find '%s' -type d | wc -l) > '%s'", src, extension_file);
    system(write_folder);

    write_extension(src, src);
}

int menu_login() {
    char username[100];
    char password[100];
    char find_account[250];
    char hash_password[250];

    printf("\n[Menu Login]\n");
    printf("Masukkan username: ");
    scanf(" %[^\n]", username);
    printf("Masukkan password: ");
    scanf(" %[^\n]", password);

    sprintf(hash_password, "echo -n '%s' | md5sum", password);

    FILE *password_hash;
    password_hash = popen(hash_password, "r");
    if (password_hash) fscanf(password_hash, "%s", password);

    sprintf(find_account, "grep -wq '%s;%s' '%s'", username, password, users_file);
    if(system(find_account) == 0) {
        if (logged_in() == 0) {
            printf("Anda masih login dengan akun lain. Silahkan logout terlebih dahulu!\n");
            return -1;
        }

        char login[250];
        sprintf(login, "echo '%s' > '%s'", username, logged_in_file);
        system(login);

        printf("Login berhasil\n");
        return 0;
    }

    printf("Akun belum terdaftar atau kata sandi salah\n");
    return -1;
}

int menu_register() {
    char username[100];
    char password[100];
    char find_username[250];
    char hash_password[250];
    char write_account[250];

    printf("\n[Menu Register]\n");
    printf("Masukkan username: ");
    scanf(" %[^\n]", username);
    printf("Masukkan password: ");
    scanf(" %[^\n]", password);

    sprintf(hash_password, "echo -n '%s' | md5sum", password);

    FILE *password_hash;
    password_hash = popen(hash_password, "r");
    if (password_hash) fscanf(password_hash, "%s", password);
    
    sprintf(find_username, "grep -oq '%s;' '%s'", username, users_file);
    if (system(find_username) == 0) {
        printf("Username telah terdaftar\n");
        return -1;
    }

    sprintf(write_account, "echo '%s;%s' >> '%s'", username, password, users_file);
    system(write_account);
    printf("Register berhasil\n");
    return 0;
}

int menu_logout() {
    if (logged_in() != 0) {
        printf("Anda belum login\n");
        return -1;
    }

    char logout[250];
    sprintf(logout, "sed -i 'd' '%s'", logged_in_file);
    system(logout);

    printf("Logout berhasil\n");
    return 0;
}

void menu(char* src) {
    int command;

    while (1) {
        printf("\n[Menu]\n");
        printf("1. Login\n");
        printf("2. Register\n");
        printf("3. Logout\n");
        printf("4. Rename\n");
        printf("5. Exit\n");
        printf("Masukkan pilihan: ");
        scanf("%d", &command);
        
        switch (command) {
            case 1: {
                menu_login();
                break;
            }
            case 2: {
                menu_register();
                break;
            }
            case 3: {
                menu_logout();
                break;
            }
            case 4: {
                rename_all(src);
                solve_e(src);
                break;
            }
            case 5: {
                return;
            }
            default: menu_login();
        }
    }
}

int main(int argc, char* argv[]) {
    init();
    if (argc > 2 && strcmp(argv[1], "-m") == 0) menu(argv[2]);
    else {
        umask(0);
        return fuse_main(argc, argv, &xmp_oper, NULL);
    }

    return 0;
}